//
//  SWTabBarViewController.m
//  Saleswhale
//
//  Created by Jun Xiu Chan on 1/21/15.
//  Copyright (c) 2015 Jun Xiu Chan. All rights reserved.
//

#import "SWTabBarViewController.h"

@interface SWTabBarViewController () <UITabBarControllerDelegate>

@end

@implementation SWTabBarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.delegate = self;
    [self createAdditionalSetup];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)createAdditionalSetup {
    for (UIViewController *vc in self.viewControllers) {
        vc.tabBarItem.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);
        vc.tabBarItem.title = @"";
    }
    
    [[UITabBar appearance] setTintColor:[UIColor colorWithRed:0.161 green:0.733 blue:0.651 alpha:1]];
}

- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController {

}

@end

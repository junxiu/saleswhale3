//
//  SWPigeon.m
//  Saleswhale
//
//  Created by Jun Xiu Chan on 1/17/15.
//  Copyright (c) 2015 Jun Xiu Chan. All rights reserved.
//

#import "SWPigeon.h"
#import "SWApiManager.h"
#import <KZPropertyMapper/KZPropertyMapper.h>

@interface SWPigeon ()
@property RLMRealm *realm;
@end

@implementation SWPigeon

+ (instancetype)flapUp {
    static SWPigeon *pigeon;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        pigeon = [[self alloc] init];
    });
    
    return pigeon;
}

- (void)composeNewPost:(SWPost *)newPost withCompletionBlock:(void (^)(NSDictionary *responseDict))completionBlock errorBlock:(void (^)(NSString *errorMessage))errorBlock{
    [[SWApiManager setup] createNote:newPost.body
                         withContact:1
                     withAttachments:nil
                 withCompletionBlock:^(NSDictionary *responseDict) {
                     completionBlock(responseDict);
                 } errorBlock:^(NSString *errorMessage) {
                     errorBlock(errorMessage);
                 }];
}

- (void)fetchPost {
    [[SWApiManager setup] refreshNewsFeedWithPageNumber:1
                                    withCompletionBlock:^(NSDictionary *responseDict) {
                                        NSArray *newsfeedArray = [responseDict safeObjectForKey:@"newsfeed"];
                                        NSMutableArray *results = [NSMutableArray array];
                                        
                                        for (NSDictionary *postDict in newsfeedArray) {
                                            NSDictionary *actor = [postDict safeObjectForKey:@"actor"];
                                            
                                            NSString *actoryType = [actor safeObjectForKey:@"actorType"];
                                            NSString *displayName = [actor safeObjectForKey:@"displayName"];
                                            NSInteger _id = [[actor safeObjectForKey:@"id"] integerValue];
                                            NSString *actorImagePath = [[[actor safeObjectForKey:@"image"] safeObjectForKey:@"thumbnail"] safeObjectForKey:@"url"];
                                            
                                            SWUser *user = [[SWUser alloc] init];
                                            user.name = displayName;
                                            user._id = _id;
                                            user.imageURLStr = actorImagePath;
                                            user.actorType = actoryType;
                                            
                                            NSInteger commentCount = [[postDict safeObjectForKey:@"comment_count"] integerValue];
                                            NSInteger interactionId = [[postDict safeObjectForKey:@"interaction_id"] integerValue];
                                            NSInteger newsfeedId = [[postDict safeObjectForKey:@"id"] integerValue];;
                                            NSString *body = [[postDict safeObjectForKey:@"object"] safeObjectForKey:@"displayName"];
                                            NSInteger bodyId = [[[postDict safeObjectForKey:@"object"] safeObjectForKey:@"id"] integerValue];
                                            NSString *bodyType = [[postDict safeObjectForKey:@"object"] safeObjectForKey:@"objectType"];
                                            NSString *dateStr = [postDict safeObjectForKey:@"published_datetime"];
                                            NSString *verb = [postDict safeObjectForKey:@"verb"];
                                            
                                            SWPost *post = [[SWPost alloc] init];
                                            post.body = body;
                                            post.commentCount = commentCount;
                                            post.interactionId = interactionId;
                                            post.newsfeedId = newsfeedId;
                                            post.bodyId = bodyId;
                                            post.bodyType = bodyType;
                                            post.date = dateStr;
                                            post.author = user;
                                            post.verb = verb;
                                            
                                            NSArray *mentions = [postDict safeObjectForKey:@"targets"];
                                            for (NSDictionary *mention in mentions) {
                                                SWUser *mentionUser = [[SWUser alloc] init];
                                                mentionUser.name = [mention safeObjectForKey:@"displayName"];
                                                mentionUser.actorType = [mention safeObjectForKey:@"targetType"];
                                                mentionUser._id = [[mention safeObjectForKey:@"id"] integerValue];
                                                mentionUser.imageURLStr = @"";
                                                [post.mentionUsers addObject:mentionUser];
                                            }
                                            
                                            [results addObject:post];
                                        }
                                        
                                        [self.delegate didFinishFetchingPosts:results];
                                    } errorBlock:^(NSString *errorMessage) {
                                        //
                                    }];
}

@end

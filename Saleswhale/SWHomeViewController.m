//
//  HomeViewController.m
//  Saleswhale
//
//  Created by Jun Xiu Chan on 1/14/15.
//  Copyright (c) 2015 Jun Xiu Chan. All rights reserved.
//

#import "SWHomeViewController.h"
#import "SWPostCollectionViewCell.h"
#import "ASHSpringyCollectionViewFlowLayout.h"
#import "SWCreateViewController.h"
#import "SWPost.h"

@interface SWHomeViewController () <UICollectionViewDataSource, UICollectionViewDelegate,SWPigeonDelegate,SWCreateViewControllerDelegate,UIScrollViewDelegate>
@property (nonatomic, strong) NSArray *posts;
@property (nonatomic, strong) SWPigeon *pigeon;
@property NSMutableArray *collectionViewCellSizes;
@property BOOL isBoot;

@end

@implementation SWHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createAdditonalSetup];
    [self fetch];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - SWPigeonDelegate

- (void)didFinishFetchingPosts:(NSArray *)results {
    self.posts = results;
    [self populateCellSizes];
}

#pragma mark - UICollectionViewDataSource, UICollectionViewDelegate

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.posts.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    SWPostCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PostCollectionViewCell" forIndexPath:indexPath];
    
    // Animation
    if (!self.isBoot) {
        [self fireBootAnimationOnCollectionViewWithCell:cell withCellIndex:indexPath.item];
    }
    
    // Populate data
    SWPost *post = (SWPost *)[self.posts objectAtIndex:indexPath.item];
    cell.post = post;
    
    // Resize body label
    CGSize bodyLabelSize = [[[self.collectionViewCellSizes objectAtIndex:indexPath.item] safeObjectForKey:@"labelSize"] CGSizeValue];
    [cell.bodyLabel setFrame:CGRectMake(65, 34, bodyLabelSize.width, bodyLabelSize.height)];
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGSize size = [[[self.collectionViewCellSizes objectAtIndex:indexPath.item] safeObjectForKey:@"cellSize"] CGSizeValue];
    return size;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    self.isBoot = YES;
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"SWCreateViewController"]) {
        SWCreateViewController *createVC = segue.destinationViewController;
        createVC.delegate = self;
    }
}

#pragma mark - SWCreateViewControllerDelegate

- (void)didPost {
    [self.collectionView setContentOffset:CGPointZero animated:YES];
    [self fetch];
}

#pragma mark - Animation

- (void)fireBootAnimationOnCollectionViewWithCell:(SWPostCollectionViewCell *)cell withCellIndex:(NSInteger)index {
    cell.alpha = 0;
    [cell setFrameOriginY:cell.bounds.origin.y - 10];
    [UIView animateWithDuration:0.65f
                          delay:0.05f * index
         usingSpringWithDamping:0.45f
          initialSpringVelocity:20
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         [cell setFrameOriginY:0];
                         cell.alpha = 1;
                     } completion:^(BOOL finished) {
                     }];
}

#pragma mark - Methods

- (void)populateCellSizes {
    self.collectionViewCellSizes = [NSMutableArray array];
    
    for (SWPost *post in self.posts) {
        const UIEdgeInsets descLabelInset = UIEdgeInsetsMake(34, 65, 45, 10);
        
        // assuming cell fills up the whole width
        CGFloat const cellWidth = [UIScreen mainScreen].bounds.size.width;
        const CGFloat descLabelMaxWidth = cellWidth - descLabelInset.left - descLabelInset.right;
        
        NSString *body = post.body;
        CGFloat descLabelMaxHeight = 100;
        CGSize descLabelMaxSize = [body getContentSizeWithFont:[UIFont fontWithName:NUNITO_LIGHT size:15] andMaxSize:CGSizeMake(descLabelMaxWidth, descLabelMaxHeight)];
        
        CGSize cellSize = CGSizeMake(cellWidth, descLabelMaxSize.height + descLabelInset.top + descLabelInset.bottom);
        
        NSMutableDictionary *cellSizeDictionary = [NSMutableDictionary dictionary];
        [cellSizeDictionary setObject:[NSValue valueWithCGSize:cellSize] forKey:@"cellSize"];
        [cellSizeDictionary setObject:[NSValue valueWithCGSize:CGSizeMake(descLabelMaxWidth, descLabelMaxSize.height)] forKey:@"labelSize"];
        
        [self.collectionViewCellSizes addObject:cellSizeDictionary];
        
    }
    
    [self.collectionView reloadData];
    
    // somehow this is dirty, but if i dont do this. flowlayout attributes will not be updated. need read apple docs regarding custom flow layout when i am free.
    
    [self setupFlowLayout];
}

- (void)createAdditonalSetup {
    [self setupFlowLayout];
}

- (void)setupFlowLayout {
    ASHSpringyCollectionViewFlowLayout *springFlowLayout = [[ASHSpringyCollectionViewFlowLayout alloc] init];
    self.collectionView.collectionViewLayout = springFlowLayout;
}

- (void)fetch {
    if (!self.pigeon) {
        self.pigeon = [SWPigeon flapUp];
        self.pigeon.delegate = self;
    }
    
    [self.pigeon fetchPost];
}

@end

//
//  NSString+StringSize.m
//  Saleswhale
//
//  Created by Jun Xiu Chan on 1/10/15.
//  Copyright (c) 2015 Jun Xiu Chan. All rights reserved.
//

#import "NSString+StringSize.h"

@implementation NSString (StringSize)

- (CGSize)getContentSizeWithFont:(UIFont *)font andMaxSize:(CGSize)maxSize {
    CGSize expectedLabelSize = CGSizeZero;
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineBreakMode = NSLineBreakByCharWrapping;
    NSDictionary *attributes = @{NSFontAttributeName:font, NSParagraphStyleAttributeName:paragraphStyle.copy};
    expectedLabelSize = [self boundingRectWithSize:CGSizeMake(maxSize.width+10, maxSize.height) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil].size;
    return CGSizeMake(expectedLabelSize.width-9, expectedLabelSize.height);
}


@end

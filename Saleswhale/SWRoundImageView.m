//
//  SWRoundImageView.m
//  Saleswhale
//
//  Created by Jun Xiu Chan on 1/9/15.
//  Copyright (c) 2015 Jun Xiu Chan. All rights reserved.
//

#import "SWRoundImageView.h"

@implementation SWRoundImageView

- (void)awakeFromNib {
    if (self.layer.cornerRadius == 0) {
        [self setCornerRadius:4];
        [self setBorderWidth:1 borderColor:[UIColor colorWithWhite:0 alpha:0.1f]];
    }
}

- (void)setCornerRadius:(CGFloat)cornerRadius {
    self.layer.cornerRadius = cornerRadius;
}

- (void)setBorderWidth:(CGFloat)width borderColor:(UIColor *)color {
    self.layer.borderColor = color.CGColor;
    self.layer.borderWidth = width;
}

@end

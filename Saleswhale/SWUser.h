//
//  SWUser.h
//  Saleswhale
//
//  Created by Jun Xiu Chan on 1/17/15.
//  Copyright (c) 2015 Jun Xiu Chan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SWUser : RLMObject

@property NSString *name;
@property NSString *imageURLStr;
@property NSInteger _id;
@property NSString *actorType;

@end

RLM_ARRAY_TYPE(SWUser)

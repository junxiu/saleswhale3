//
//  SWPigeon.h
//  Saleswhale
//
//  Created by Jun Xiu Chan on 1/17/15.
//  Copyright (c) 2015 Jun Xiu Chan. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "SWPost.h"
#import "SWUser.h"

@protocol SWPigeonDelegate <NSObject>

- (void)didFinishFetchingPosts:(NSArray *)results;

@end

@interface SWPigeon : NSObject
@property (nonatomic) id <SWPigeonDelegate> delegate;

+ (instancetype)flapUp;
- (void)fetchPost;
- (void)composeNewPost:(SWPost *)newPost withCompletionBlock:(void (^)(NSDictionary *responseDict))completionBlock errorBlock:(void (^)(NSString *errorMessage))errorBlock;

@end

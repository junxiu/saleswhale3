//
//  SWCreateViewController.h
//  Saleswhale
//
//  Created by Jun Xiu Chan on 1/21/15.
//  Copyright (c) 2015 Jun Xiu Chan. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SWCreateViewControllerDelegate <NSObject>

- (void)didPost;

@end

@interface SWCreateViewController : UIViewController

@property (nonatomic) id <SWCreateViewControllerDelegate> delegate;
@property (weak, nonatomic) IBOutlet UITextView *bodyTextView;

- (IBAction)send:(id)sender;
- (IBAction)dismiss:(id)sender;

@end

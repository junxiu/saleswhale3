//
//  SWPost.h
//  Saleswhale
//
//  Created by Jun Xiu Chan on 1/17/15.
//  Copyright (c) 2015 Jun Xiu Chan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SWUser.h"

@interface SWPost : RLMObject

@property SWUser *author;

@property NSString *body;
@property NSInteger starCount;
@property NSInteger commentCount;
@property NSInteger interactionId;
@property NSInteger newsfeedId;
@property NSInteger bodyId;
@property NSString *bodyType;
@property NSString *date;
@property NSString *verb;
@property BOOL isStarred;
@property RLMArray <SWUser> *mentionUsers;

@end

RLM_ARRAY_TYPE(SWPost)

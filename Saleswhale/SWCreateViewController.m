//
//  SWCreateViewController.m
//  Saleswhale
//
//  Created by Jun Xiu Chan on 1/21/15.
//  Copyright (c) 2015 Jun Xiu Chan. All rights reserved.
//

#import "SWCreateViewController.h"
#import "SWPost.h"

@interface SWCreateViewController ()

@end

@implementation SWCreateViewController

#pragma mark - Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated {
    [self.bodyTextView becomeFirstResponder];
}

#pragma mark - IBActions

- (IBAction)send:(id)sender {
    [self compose];
}

#pragma mark - API Call

- (void)compose {
    [SVProgressHUD showWithStatus:@"Loading" maskType:SVProgressHUDMaskTypeBlack];
    SWPost *newPost = [[SWPost alloc] init];
    [newPost setBody:self.bodyTextView.text];
    [[SWPigeon flapUp] composeNewPost:newPost
                  withCompletionBlock:^(NSDictionary *responseDict) {
                      [SVProgressHUD showSuccessWithStatus:@"Done"];
                      [self.delegate didPost];
                      [self dismiss:nil];
                  } errorBlock:^(NSString *errorMessage) {
                      [SVProgressHUD showErrorWithStatus:errorMessage];
                      [self dismiss:nil];
                  }];
}

#pragma mark - Methods

- (IBAction)dismiss:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end

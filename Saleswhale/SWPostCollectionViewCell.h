//
//  PostCollectionViewCell.h
//  Saleswhale
//
//  Created by Jun Xiu Chan on 1/6/15.
//  Copyright (c) 2015 Jun Xiu Chan. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SWRoundImageView.h"
#import "SWPost.h"

@interface SWPostCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet SWRoundImageView *authorImageView;
@property (weak, nonatomic) IBOutlet UILabel *authorLabel;
@property (weak, nonatomic) IBOutlet UIButton *commentButton;
@property (weak, nonatomic) IBOutlet UIButton *starButton;
@property (weak, nonatomic) IBOutlet UILabel *bodyLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (nonatomic, strong) SWPost *post;
@property (weak, nonatomic) IBOutlet UIButton *interactionTypeButton;
@property (weak, nonatomic) IBOutlet UIView *separatorView;

@end

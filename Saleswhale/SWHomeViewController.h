//
//  HomeViewController.h
//  Saleswhale
//
//  Created by Jun Xiu Chan on 1/14/15.
//  Copyright (c) 2015 Jun Xiu Chan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SWHomeViewController : UIViewController
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@end

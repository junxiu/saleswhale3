//
//  OmuraApiManager.h
//  OmuraAPI
//
//  Created by Jun Xiu Chan on 12/8/14.
//  Copyright (c) 2014 gettingreal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppAPI.h"

@interface SWApiManager : AppAPI

+ (instancetype)setup;

- (void)createNote:(NSString *)note withContact:(NSInteger)contactID withAttachments:(NSArray *)attachments withCompletionBlock:(void (^)(NSDictionary *responseDict))completionBlock errorBlock:(void (^)(NSString *errorMessage))errorBlock;

- (void)attachFile:(NSData *)fileData withFileName:(NSString *)fileName andFileType:(NSString *)fileType toNote:(NSInteger)noteId;

- (void)refreshNewsFeedWithPageNumber:(NSInteger)pageNumber
                  withCompletionBlock:(void (^)(NSDictionary *responseDict))completionBlock
                           errorBlock:(void (^)(NSString *errorMessage))errorBlock;
@end

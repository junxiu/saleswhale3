//
//  OmuraApiManager.m
//  OmuraAPI
//
//  Created by Jun Xiu Chan on 12/8/14.
//  Copyright (c) 2014 gettingreal. All rights reserved.
//

#import "SWApiManager.h"

@interface SWApiManager () {
}

@end
@implementation SWApiManager

+ (instancetype)setup {
    static SWApiManager *apiManager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        apiManager = [[self alloc] init];
    });
    return apiManager;
}

- (void)createNote:(NSString *)note
       withContact:(NSInteger)contactID
   withAttachments:(NSArray *)attachments
withCompletionBlock:(void (^)(NSDictionary *responseDict))completionBlock
        errorBlock:(void (^)(NSString *errorMessage))errorBlock {
    
    /*
    {
        "auth_token" : "Bnykjzgsmza3cGMt9qBV",
        "note" : {
            "body" : "This is a note that contains a whale."
        }
    }
    */
    
    /*
        POST http://saleswhale-v2-env-tcqj2t2dst.elasticbeanstalk.com/api/v1/notes
     */
    
    [self requestWithPath:@"notes"
               parameters:@{@"auth_token":[self getUserAuthToken],
                             @"note":@{@"body":note,
                                       @"contact_id":[NSNumber numberWithInteger:contactID]}
                            }
                   method:AFRequestMethodPost
               sslEnabled:NO
          completionBlock:^(NSMutableDictionary *responseDict) {
              //completed
              
              completionBlock(responseDict);
              
              if (attachments.count != 0) {
                  for (NSDictionary *fileDictionary in attachments) {
                  }
                  
              }
              
          } errorBlock:^(NSString *errorMessage) {
              //error
          }];
    
}

- (void)attachFile:(NSData *)fileData withFileName:(NSString *)fileName andFileType:(NSString *)fileType toNote:(NSInteger)noteId {
    
    /*
    {
        "auth_token" : "Bnykjzgsmza3cGMt9qBV",
        "asset" : form-data
    }
    */
    
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@/%@/notes/%li/file_assets",APP_HOSTNAME,VERSION,(long)noteId]]];

    AFHTTPRequestOperation *op = [manager POST:@""
                                    parameters:@{@"auth_token":[self getUserAuthToken]}
                     constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
                         
        [formData appendPartWithFileData:fileData name:@"asset" fileName:fileName mimeType:fileType];
                         
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Success: %@ ***** %@", operation.responseString, responseObject);
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@ ***** %@", operation.responseString, error);
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    }];
    
    [op start];
    
}

- (void)refreshNewsFeedWithPageNumber:(NSInteger)pageNumber
                  withCompletionBlock:(void (^)(NSDictionary *responseDict))completionBlock
                           errorBlock:(void (^)(NSString *errorMessage))errorBlock{
    /*
     
     GET http://saleswhale-v2-env-tcqj2t2dst.elasticbeanstalk.com/api/v1/newsfeed?auth_token=NP1SyHzQzdZ1nvnqivE-
     
     PARAMS
     auth_token	The authentication token to identify the user
     page	The page number to retrieve (default - returns 10 newsfeed items)
     
     */
    
    [self requestWithPath:@"newsfeed"
               parameters:@{@"auth_token":[self getUserAuthToken],
                            }
                   method:AFRequestMethodGet
               sslEnabled:NO
          completionBlock:^(NSMutableDictionary *responseDict) {
              completionBlock(responseDict);
          } errorBlock:^(NSString *errorMessage) {
              errorBlock(errorMessage);
          }];
}

- (NSString *)getUserAuthToken {
    return @"NP1SyHzQzdZ1nvnqivE-";
}

@end

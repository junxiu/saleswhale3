//
//  PostCollectionViewCell.m
//  Saleswhale
//
//  Created by Jun Xiu Chan on 1/6/15.
//  Copyright (c) 2015 Jun Xiu Chan. All rights reserved.
//

#import "SWPostCollectionViewCell.h"

@implementation SWPostCollectionViewCell

- (void)setPost:(SWPost *)post {
    self.bodyLabel.text = post.body;
    self.authorLabel.text = post.author.name;
    [self.authorLabel setFrameWidth:self.frame.size.width];
    [self.authorLabel sizeToFit];
    [self.authorLabel setFrameOriginX:65 Y:11];
    
    [self.starButton setTitle:[NSString stringWithFormat:@"  %li",(long)post.starCount] forState:UIControlStateNormal];
    [self.commentButton setTitle:[NSString stringWithFormat:@"  %li",(long)post.commentCount] forState:UIControlStateNormal];
    
    if (post.isStarred) {
        self.starButton.tintColor = [UIColor colorWithRed:0.945 green:0.776 blue:0.094 alpha:1];
    }
    else {
        self.starButton.tintColor = [UIColor colorWithRed:0.945 green:0.776 blue:0.094 alpha:1];
    }
    
    self.authorImageView.image = nil;
    self.authorImageView.backgroundColor = [UIColor colorWithRed:0.149 green:0.149 blue:0.200 alpha:1];
    [self.authorImageView setImageWithURL:[NSURL URLWithString:post.author.imageURLStr]
                                completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                    self.authorImageView.backgroundColor = [UIColor clearColor];
                                    self.authorImageView.image = image;
                                }
              usingActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    
    NSString *interactionTypeStr;
    
    if ([post.verb isEqualToString:@"logged_meeting"]) {
        interactionTypeStr = @"MEETING";
        self.interactionTypeButton.backgroundColor = [UIColor flatSkyBlueColor];
    }
    else if ([post.verb isEqualToString:@"emailed"]) {
        interactionTypeStr = @"EMAIL";
        self.interactionTypeButton.backgroundColor = [UIColor colorWithRed:200/255.f green:200/255.f blue:200/255.f alpha:1];
    }
    else {
        interactionTypeStr = @"";
        self.interactionTypeButton.backgroundColor = [UIColor clearColor];
    }
    
    self.interactionTypeButton.tintColor = [UIColor colorWithRed:46/255.f green:46/255.f blue:46/255.f alpha:1];
    
    [self.interactionTypeButton setTitle:interactionTypeStr forState:UIControlStateNormal];
    [self.interactionTypeButton setFrameWidth:60 height:19];
    [self.interactionTypeButton sizeToFit];
    [self.interactionTypeButton setFrameWidth:self.interactionTypeButton.frame.size.width + 10 height:19];
    [self.interactionTypeButton setFrameOriginX:CGRectGetMaxX(self.authorLabel.frame) + 10 Y:12];
    [self.interactionTypeButton createBordersWithColor:[UIColor clearColor] withCornerRadius:4 andWidth:0];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"dd-MM-yyyy HH:mm";
    
    NSDate *date = [dateFormatter dateFromString:post.date.dateFromTimestamp];
    
    self.dateLabel.text = [[NSDate date] shortTimeAgoSinceDate:date];
    
    [self.separatorView setFrameHeight:0.5f];
}

@end